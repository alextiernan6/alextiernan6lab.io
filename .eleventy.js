const sass = require('sass');
const fs = require('fs');

const filterDir = './filters'
const moment = require(`${filterDir}/moment.js`);
const inclusiveLangPlugin = require("@11ty/eleventy-plugin-inclusive-language");

module.exports = function(eleventyConfig) {
  const result = sass.renderSync({
    file: "theme/sass/style.scss",
    includePaths: ["node_modules/materialize-css/sass/"],
    outputStyle: "compressed",
  });

  if (!fs.existsSync("public")) {
    fs.mkdirSync("public");
  }
  fs.writeFileSync("public/style.css", result.css.toString());

  eleventyConfig.addFilter('moment', moment);
  eleventyConfig.addPlugin(inclusiveLangPlugin);

  eleventyConfig.addWatchTarget("./theme/sass/**/*.scss");
  eleventyConfig.setBrowserSyncConfig({
		files: './public/style.css'
	})
  
  const copyExtensions = ['jpg','jpeg','png','gif'];
  for (ext of copyExtensions) {
    eleventyConfig.addPassthroughCopy(`content/**/*.${ext}`);
  }

  const jsPassthrough = {};
  jsPassthrough[require.resolve('materialize-css')] = '/materialize.js';
  eleventyConfig.addPassthroughCopy(jsPassthrough);

  return {
    dir: {
      input: "content",
      output: "public",
      includes: "../theme/layout",
    }
  };
};
