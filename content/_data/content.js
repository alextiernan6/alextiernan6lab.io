module.exports = {
  CI_PIPELINE_URL: process.env.CI_PIPELINE_URL,
  CI_PIPELINE_ID: process.env.CI_PIPELINE_ID,
  now: new Date(),
};
