---
title: PocketLab v1.3.0 Released
blurb: Today the New version of PocketLab, a mobile @gitlab client, is out now for ios and android!
author: alextiernan6
hero: hero.jpg
---

PocketLab, an [open source mobile gitlab client](https://gitlab.com/alextiernan6/pocketlab), v1.3 is now available on the App Store and Play Store. This release brings some new features and some bug fixes.

Got some feedback, or want a feature adding, create an issue on [GitLab](https://gitlab.com/alextiernan6/pocketlab/issues)

### Changelog
*  ✅ Accept Merge Requests
*  👁️ View Merge Request Pipelines
*  🐞 General Bug Fixes
