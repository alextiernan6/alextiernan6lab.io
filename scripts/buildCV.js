const puppeteer = require('puppeteer');
const scrollPageToBottom = require('puppeteer-autoscroll-down');
const waitPort = require('wait-port');

waitPort({host:'localhost', port:8080})
  .then(async (open) => {
    if (open) {
      console.log('Launching puppeteer');
      const browser = await puppeteer.launch({
        args: [
          // Required for Docker version of Puppeteer
          '--no-sandbox',
          '--disable-setuid-sandbox',
          // This will write shared memory files into /tmp instead of /dev/shm,
          // because Docker’s default for /dev/shm is 64MB
          '--disable-dev-shm-usage'
        ]
      });

      try {
        const browserVersion = await browser.version()
        console.log(`Started ${browserVersion}`);

        const page = await browser.newPage();
        console.log('Opened new page');

        await page.goto('http://localhost:8080/cv');
        console.log('Loaded http://localhost:8080/cv');

        /*
            Chrome has a weird printer bug where it only sets the background colour
            for areas that have been viewed in the browser. This way we make sure
            the entire page has been viewed before generating the PDF.
        */
        await scrollPageToBottom(page);
        console.log('Scrolled to bottom');

        await page.pdf({
          path: 'public/cv/Alex Tiernan-Berry _ CV.pdf',
          format: 'A4',
          printBackground: true,
        });
        console.log('Generated PDF');

        await browser.close();
        console.log('Closing browser');
        process.exit();
      } catch (e) {
        console.log(e);
        process.exit();
      } finally {
        browser.close();
      }
    } else {
      console.error('localhost:8080 failed to open');
      process.exit();
    }
  })
  .catch((err) => {
    console.log(`An unknown error occured while waiting for the port: ${err}`);
    process.exit();
  });
